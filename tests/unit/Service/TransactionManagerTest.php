<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\Product;
use App\Entity\Transaction;
use App\Service\TransactionManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class TransactionManagerTest extends TestCase
{
    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * @var EntityManagerInterface|MockObject
     */
    private $em;

    public function setUp(): void
    {
        $this->em = $this->createMock(EntityManagerInterface::class);
        $this->transactionManager = new TransactionManager($this->em);
        parent::setUp();
    }

    /**
     * @dataProvider provides_expected_change_displays
     */
    public function testItDisplaysChangeCorrectly(int $change, string $expectedOutput)
    {
        $productMock = $this->createMock(Product::class);

        $productMock->method('getCurrency')->willReturn('RON');

        $transaction = (new Transaction())
            ->setBalance(0)
            ->setPaid(true)
            ->setProduct($productMock)
            ->setChange($change);

        $display = $this->transactionManager->displayChange($transaction);

        self::assertEquals($expectedOutput, $display);
    }

    public function provides_expected_change_displays()
    {
        return [
            [8, '8 RON (1x5 RON, 3x1 RON)'],
            [16, '16 RON (1x10 RON, 1x5 RON, 1x1 RON)'],
            [11, '11 RON (1x10 RON, 1x1 RON)'],
            [1, '1 RON (1x1 RON)'],
        ];
    }
}
