<?php

namespace App\Command;

use App\Payload\CoffeeMachinePayload;
use App\Service\LockManager;
use Exception;
use League\Tactician\CommandBus;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CoffeeMachineCommand extends Command
{
    protected static $defaultName = 'app:coffee:buy';

    /**
     * @var LockManager
     */
    private $lockManager;

    /**
     * @var CommandBus
     */
    private $coffeeMachineCommandBus;

    public function __construct(
        LockManager $lockManager,
        CommandBus $commandBus
    ) {
        $this->lockManager = $lockManager;
        $this->coffeeMachineCommandBus = $commandBus;
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('vendorId', InputArgument::REQUIRED, 'Which vendor machine you want to use? (used for lock generation)');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $lock = $this->lockManager->createLock($input->getArgument('vendorId'));

        if ($lock->acquire()) {
            try {
                $payload = new CoffeeMachinePayload($input, $output, $this->getHelper('question'));
                $this->coffeeMachineCommandBus->handle($payload);
            } catch (Exception $exception) {
                $output->writeln($exception->getMessage());
            }
        } else {
            $output->writeln('This vendor machine is already in use');

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
