<?php

namespace App\EventSubscriber;

use App\Event\TransactionPaidInFullEvent;
use App\Repository\InventoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UpdateInventorySubscriber implements EventSubscriberInterface
{
    /**
     * @var InventoryRepository
     */
    private $inventoryRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(InventoryRepository $inventoryRepository, EntityManagerInterface $entityManager)
    {
        $this->inventoryRepository = $inventoryRepository;
        $this->entityManager = $entityManager;
    }

    public function handleTransactionPaidInFullEvent(TransactionPaidInFullEvent $event)
    {
        $ingredients = $event->getTransaction()->getProduct()->getRecipe()->getIngredients();

        foreach ($ingredients as $ingredient) {
            $inventory = $this->inventoryRepository->findOneBy(['name' => $ingredient->getName()]);

            if (null === $inventory) {
                continue;
            }

            $inventory->setQuantity(
                $inventory->getQuantity() - $ingredient->getQuantityPerServing()
            );

            $this->entityManager->persist($inventory);
            $this->entityManager->flush();
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            TransactionPaidInFullEvent::class => 'handleTransactionPaidInFullEvent',
        ];
    }
}
