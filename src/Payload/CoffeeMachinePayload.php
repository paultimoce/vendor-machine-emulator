<?php

declare(strict_types=1);

namespace App\Payload;

use App\Entity\Transaction;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CoffeeMachinePayload
{
    /**
     * @var InputInterface
     */
    private $input;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * @var QuestionHelper
     */
    private $questionHelper;

    public function __construct(InputInterface $input, OutputInterface $output, QuestionHelper $questionHelper)
    {
        $this->input = $input;
        $this->output = $output;
        $this->questionHelper = $questionHelper;
        $this->transaction = new Transaction();
    }

    public function getTransaction(): Transaction
    {
        return $this->transaction;
    }

    public function getInput(): InputInterface
    {
        return $this->input;
    }

    public function getOutput(): OutputInterface
    {
        return $this->output;
    }

    public function getQuestionHelper(): QuestionHelper
    {
        return $this->questionHelper;
    }
}
