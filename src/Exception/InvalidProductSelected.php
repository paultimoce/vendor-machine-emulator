<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;
use Throwable;

class InvalidProductSelected extends Exception
{
    const MESSAGE = 'The product selected is invalid';

    public function __construct(string $message = null, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message ?? self::MESSAGE, $code, $previous);
    }
}
