<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;
use Throwable;

class OutOfStockException extends Exception
{
    const MESSAGE = 'This product is out of stock';

    public function __construct(string $message = null, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message ?? self::MESSAGE, $code, $previous);
    }
}
