<?php

namespace App\DataFixtures;

use App\Entity\Ingredient;
use App\Entity\Inventory;
use App\Entity\Product;
use App\Entity\Recipe;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CoffeeFixtures extends Fixture
{
    const COFFEE = 'coffee';
    const WATER = 'water';
    const MILK = 'milk';
    const SUGAR = 'sugar';
    const VODKA = 'vodka';

    public function load(ObjectManager $manager)
    {
        $inventories = [
            (new Inventory())->setName(self::COFFEE)->setQuantity(1000)->setUnit('ml'),
            (new Inventory())->setName(self::WATER)->setQuantity(1000)->setUnit('ml'),
            (new Inventory())->setName(self::MILK)->setQuantity(1000)->setUnit('ml'),
            (new Inventory())->setName(self::SUGAR)->setQuantity(100)->setUnit('g'),
            (new Inventory())->setName(self::VODKA)->setQuantity(100)->setUnit('ml'),
        ];

        foreach ($inventories as $inventory) {
            $manager->persist($inventory);
        }

        $coffee5 = (new Ingredient())->setName(self::COFFEE)->setQuantityPerServing(5)->setUnit('ml');
        $water30 = (new Ingredient())->setName(self::WATER)->setQuantityPerServing(30)->setUnit('ml');
        $milk0 = (new Ingredient())->setName(self::MILK)->setQuantityPerServing(0)->setUnit('ml');
        $sugar2 = (new Ingredient())->setName(self::SUGAR)->setQuantityPerServing(2)->setUnit('g');
        $milk30 = (new Ingredient())->setName(self::MILK)->setQuantityPerServing(30)->setUnit('ml');
        $vodka10 = (new Ingredient())->setName(self::VODKA)->setQuantityPerServing(10)->setUnit('ml');

        $expresoRecipe = (new Recipe())
            ->setName('expreso')
            ->addIngredient($coffee5)
            ->addIngredient($water30)
            ->addIngredient($milk0)
            ->addIngredient($sugar2);

        $irishCappucino = (new Recipe())
            ->setName('irish capucino')
            ->addIngredient($coffee5)
            ->addIngredient($water30)
            ->addIngredient($milk30)
            ->addIngredient($vodka10);

        $products = [
            (new Product())->setPrice(10)->setCurrency('RON')->setRecipe($expresoRecipe),
            (new Product())->setPrice(15)->setCurrency('RON')->setRecipe($irishCappucino),
        ];

        foreach ($products as $product) {
            $manager->persist($product);
        }

        $manager->flush();
    }
}
