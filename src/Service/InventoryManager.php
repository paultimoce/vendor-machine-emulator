<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;
use App\Repository\InventoryRepository;

class InventoryManager
{
    /**
     * @var InventoryRepository
     */
    private $inventoryRepository;

    public function __construct(InventoryRepository $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
    }

    public function isOutOfStock(Product $product): bool
    {
        foreach ($product->getRecipe()->getIngredients() as $ingredient) {
            $ingredientInventory = $this->inventoryRepository->findOneBy(['name' => $ingredient->getName()]);

            if ($ingredient->getUnit() !== $ingredientInventory->getUnit()) {
                continue;
            }

            if (null === $ingredientInventory || $ingredientInventory->getQuantity() < $ingredient->getQuantityPerServing()) {
                return true;
            }
        }

        return false;
    }
}
