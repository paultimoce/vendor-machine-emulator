<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\LockInterface;
use Symfony\Component\Lock\Store\FlockStore;

class LockManager
{
    /**
     * @var LockFactory
     */
    private $lockFactory;

    public function __construct()
    {
        $this->lockFactory = new LockFactory(new FlockStore());
    }

    public function createLock(string $vendorId): LockInterface
    {
        return $this->lockFactory->createLock($vendorId);
    }
}
