<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;
use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;

class TransactionManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createTransaction(Product $product, string $paymentMethod): Transaction
    {
        $transaction = (new Transaction())
            ->setProduct($product)
            ->setBalance($product->getPrice())
            ->setPaymentMethod($paymentMethod)
            ->setPaid(false);

        $this->entityManager->persist($transaction);
        $this->entityManager->flush();

        return $transaction;
    }

    public function updateTransaction(Transaction $transaction, int $amountPaid): Transaction
    {
        $newBalance = $transaction->getBalance() - $amountPaid;

        if ($newBalance <= 0) {
            return $transaction
                ->setChange(abs($newBalance))
                ->setBalance(0)
                ->setPaid(true);
        }

        return $transaction->setBalance($newBalance);
    }

    public function displayChange(Transaction $transaction): string
    {
        $change = $transaction->getChange();
        $currency = $transaction->getProduct()->getCurrency();

        $allowedMoneyUnits = [10, 5, 1];

        $display = [];

        $numOfUnits = 0;
        $updatedChange = $change;
        foreach ($allowedMoneyUnits as $unit) {
            if ($unit > $updatedChange) {
                $numOfUnits = 0;
                continue;
            }

            while ($updatedChange > 0) {
                ++$numOfUnits;
                $updatedChange = $updatedChange - $unit;
                $display[$unit] = $numOfUnits.'x'.$unit.' '.$currency;

                if ($unit > $updatedChange) {
                    $numOfUnits = 0;
                    break;
                }
            }
        }

        return (string) $change.' '.$currency.' ('.implode(', ', $display).')';
    }
}
