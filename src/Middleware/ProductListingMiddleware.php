<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Entity\Product;
use App\Exception\InvalidProductSelected;
use App\Payload\CoffeeMachinePayload;
use App\Service\ProductManager;
use League\Tactician\Middleware;
use Symfony\Component\Console\Question\ChoiceQuestion;

class ProductListingMiddleware implements Middleware
{
    /**
     * @var ProductManager
     */
    private $productManager;

    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * @param CoffeeMachinePayload $command
     *
     * @return mixed
     *
     * @throws InvalidProductSelected
     */
    public function execute($command, callable $next)
    {
        $command->getOutput()->writeln([
            'Welcome to the coffee machine vendor app',
            '============',
            'What kind of coffee would you like today?',
            '',
        ]);

        $productSelected = $this->askForSelectedProduct($command, $this->productManager->retrieveProducts());

        $command->getTransaction()
            ->setProduct($productSelected)
            ->setPaid(false)
            ->setBalance($productSelected->getPrice())
            ->setChange(0);

        return $next($command);
    }

    private function askForSelectedProduct(CoffeeMachinePayload $payload, array $products): ?Product
    {
        $selectProductQuestion = new ChoiceQuestion(
            'What kind of coffee would you like today?',
            array_values($products)
        );

        return $payload->getQuestionHelper()->ask(
            $payload->getInput(),
            $payload->getOutput(),
            $selectProductQuestion
        );
    }
}
