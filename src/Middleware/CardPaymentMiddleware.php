<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Entity\Transaction;
use App\Event\TransactionPaidInFullEvent;
use App\Payload\CoffeeMachinePayload;
use App\Service\TransactionManager;
use Doctrine\ORM\EntityManagerInterface;
use League\Tactician\Middleware;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class CardPaymentMiddleware implements Middleware
{
    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        TransactionManager $transactionManager,
        EventDispatcherInterface $eventDispatcher,
        EntityManagerInterface $entityManager
    ) {
        $this->transactionManager = $transactionManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     * @param CoffeeMachinePayload $command
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        if (Transaction::CARD === $command->getTransaction()->getPaymentMethod()) {
            $command->getOutput()->writeln('Please follow ATM instructions!');

            // ... aici astepti raspuns de la ATM pentru confirmarea platii
            sleep(5);

            $command->getTransaction()
                ->setBalance(0)
                ->setPaid(true);

            $this->eventDispatcher->dispatch(
                new TransactionPaidInFullEvent($command->getTransaction())
            );
            $this->entityManager->persist($command->getTransaction());
            $this->entityManager->flush();
            $command->getOutput()->writeln('Thank you for your business!');
        }

        return $next($command);
    }
}
