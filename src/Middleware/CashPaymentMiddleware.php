<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Entity\Transaction;
use App\Event\TransactionPaidInFullEvent;
use App\Payload\CoffeeMachinePayload;
use App\Service\TransactionManager;
use Doctrine\ORM\EntityManagerInterface;
use League\Tactician\Middleware;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class CashPaymentMiddleware implements Middleware
{
    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        TransactionManager $transactionManager,
        EventDispatcherInterface $eventDispatcher,
        EntityManagerInterface $entityManager
    ) {
        $this->transactionManager = $transactionManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     * @param CoffeeMachinePayload $command
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        $transaction = $command->getTransaction();
        if (Transaction::CASH === $transaction->getPaymentMethod()) {
            while (!$transaction->isPaid()) {
                $money = $this->askForMoney(
                    $command,
                    (string) $transaction->getBalance().' '.$transaction->getProduct()->getCurrency()
                );

                $this->transactionManager->updateTransaction($transaction, $money);

                if ($transaction->isPaid()) {
                    $this->eventDispatcher->dispatch(new TransactionPaidInFullEvent($transaction));

                    if ($transaction->getChange() > 0) {
                        $command->getOutput()->writeln(
                            'Please pick up your change of '.$this->transactionManager->displayChange($transaction)
                        );
                    }

                    $this->entityManager->persist($command->getTransaction());
                    $this->entityManager->flush();
                    $command->getOutput()->writeln('Thank you for your business!');
                }
            }
        }

        return $next($command);
    }

    private function askForMoney(CoffeeMachinePayload $payload, string $remaingBalance): int
    {
        $moneyQuestion = new ChoiceQuestion(
            'Please enter your money into the vending machine. Outstanding balance: '.$remaingBalance,
            ['1 RON', '5 RON', '10 RON']
        );

        $moneyEntered = $payload->getQuestionHelper()->ask(
            $payload->getInput(),
            $payload->getOutput(),
            $moneyQuestion
        );

        return (int) explode(' ', $moneyEntered)[0];
    }
}
