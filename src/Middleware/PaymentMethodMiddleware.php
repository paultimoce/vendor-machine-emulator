<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Entity\Transaction;
use App\Payload\CoffeeMachinePayload;
use League\Tactician\Middleware;
use Symfony\Component\Console\Question\ChoiceQuestion;

class PaymentMethodMiddleware implements Middleware
{
    /**
     * @param CoffeeMachinePayload $command
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        $paymentMethodQuestion = new ChoiceQuestion(
            'How would you like to pay for your coffee?',
            [Transaction::CASH, Transaction::CARD]
        );

        $paymentMethod = (string) $command->getQuestionHelper()->ask(
            $command->getInput(),
            $command->getOutput(),
            $paymentMethodQuestion
        );

        $command->getTransaction()->setPaymentMethod($paymentMethod);

        return $next($command);
    }
}
