<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Exception\OutOfStockException;
use App\Payload\CoffeeMachinePayload;
use App\Service\InventoryManager;
use League\Tactician\Middleware;

class InventoryMiddleware implements Middleware
{
    /**
     * @var InventoryManager
     */
    private $inventoryManager;

    public function __construct(InventoryManager $inventoryManager)
    {
        $this->inventoryManager = $inventoryManager;
    }

    /**
     * @param CoffeeMachinePayload $command
     *
     * @return mixed
     *
     * @throws OutOfStockException
     */
    public function execute($command, callable $next)
    {
        if ($this->inventoryManager->isOutOfStock($command->getTransaction()->getProduct())) {
            throw new OutOfStockException();
        }

        return $next($command);
    }
}
